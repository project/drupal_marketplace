<?php

/**
 * @file
 * Contains module_entity.page.inc.
 *
 * Page callback for Module entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Module entity templates.
 *
 * Default template: module_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_module_entity(array &$variables) {
  // Fetch ModuleEntity Entity Object.
  $module_entity = $variables['elements']['#module_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
