<?php

namespace Drupal\drupal_marketplace;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Module entity entity.
 *
 * @see \Drupal\drupal_marketplace\Entity\ModuleEntity.
 */
class ModuleEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\drupal_marketplace\Entity\ModuleEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished module entity entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published module entity entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit module entity entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete module entity entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add module entity entities');
  }

}
