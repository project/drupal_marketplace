<?php

namespace Drupal\drupal_marketplace\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Processes Drupal.org modules API for Drupal Marketplace Module.
 *
 * @QueueWorker(
 *   id = "drupal-marketplace-module",
 *   title = @Translation("Drupal marketplace module queue worker"),
 *   cron = {"time" = 10}
 * )
 */
class DrupalMarketplaceModuleQueueWorker extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($module) {
    $service = \Drupal::service('drupal_marketplace.parser');
    $service->makeRequest($module, 'XML');
    $response = $service->getResponse();
    if (!empty($response->releases->release[0])) {
      $release = $response->releases->release[0];
      $download_link = (string) $release->download_link;
      $storage = \Drupal::entityTypeManager()->getStorage('module_entity');
      $entities = $storage->loadByProperties(['name' => $response->short_name]);
      $entity = reset($entities);
      if (!empty($entity)) {
        $entity->setDownloadLink($download_link);
        $entity->save();
      }
    }
  }

}
