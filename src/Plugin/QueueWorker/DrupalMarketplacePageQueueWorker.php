<?php

namespace Drupal\drupal_marketplace\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Processes Drupal.org modules API for Drupal Marketplace Module.
 *
 * @QueueWorker(
 *   id = "drupal-marketplace-page",
 *   title = @Translation("Drupal marketplace page queue worker"),
 *   cron = {"time" = 10}
 * )
 */
class DrupalMarketplacePageQueueWorker extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($page) {
    $service = \Drupal::service('drupal_marketplace.parser');
    $url = $service->constructPageUrl($page);
    $service->makeRequest($url);
    $response = $service->getResponse();
    if (!empty($response->list)) {
      foreach ($response->list as $item) {
        if (empty($item->project_usage)) {
          continue;
        }
        $skip = TRUE;
        foreach ($item->project_usage as $key => $value) {
          if ($key === '8.x') {
            $skip = FALSE;
          }
        }
        if ($skip) {
          continue;
        }


        $storage = \Drupal::entityTypeManager()->getStorage('module_entity');
        $entities = $storage->loadByProperties(['name' => $item->field_project_machine_name]);

        $entity = reset($entities);
        if (empty($entity)) {
          $entity = $storage->create();
          $entity->setName($item->field_project_machine_name);
          $entity->setMachineName($item->field_project_machine_name);
          $entity->setSummary($item->field_project_machine_name);
          $entity->setModuleAuthor('IAM');
          $entity->save();
        }


        /** @var QueueFactory $queue_factory */
        $queue_factory = \Drupal::service('queue');
        /** @var QueueInterface $queue */
        $queue = $queue_factory->get('drupal-marketplace-module');
        $module_url = $service->constructModuleUrl($item);
        $queue->createItem($module_url);
      }
    }

  }

}
