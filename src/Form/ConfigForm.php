<?php

namespace Drupal\drupal_marketplace\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class lesson3Form.
 *
 * @package Drupal\lesson3\Form
 */
class ConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['drupal_marketplace.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'drupal_marketplace_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('drupal_marketplace.settings');

    $form['quantity'] = [
      '#type' => 'number',
      '#title' => $this->t('Quantity'),
      '#default_value' => $config->get('quantity'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    \Drupal::configFactory()->getEditable('drupal_marketplace.settings')
      ->set('quantity', $form_state->getValue('quantity'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
