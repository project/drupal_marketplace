<?php

namespace Drupal\drupal_marketplace\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Module entity entities.
 *
 * @ingroup drupal_marketplace
 */
class ModuleEntityDeleteForm extends ContentEntityDeleteForm {


}
