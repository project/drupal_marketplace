<?php

namespace Drupal\drupal_marketplace\Service;

/**
 * Class DrupalMarketPlaceParserService.
 *
 * @package Drupal\drupal_marketplace\Service
 */
class DrupalMarketPlaceParserService {

  private const DRUPAL_MARKETPLACE_API_PAGE_URL = 'https://www.drupal.org/api-d7/node.json?type=project_module&page=__PAGE__';

  private const DRUPAL_MARKETPLACE_API_MODULE_URL = 'https://updates.drupal.org/release-history/__MODULE_MACHINE_NAME__/8.x';

  private $client;

  private $query;

  private $logger;

  private $headers;

  private $response;

  private $config;

  private $firstPage;

  private $lastPage;

  /**
   * Drupal-marketplace Parser Service constructor.
   */
  public function __construct() {
    $configFactory = \Drupal::configFactory();
    $this->config = $configFactory->get('drupal-marketplace.settings');
    $this->setHeaders('Accept', 'application/json');
    $this->client = \Drupal::httpClient();
    $this->logger = \Drupal::logger('drupal-marketplace');
  }

  /**
   * Get modules from drupal.org API.
   *
   * @return object
   *   Return array of modules or null.
   */
  public function parseModules() {
    $this->makeRequest($this->config->get('api_base'));
    $this->parsePages();
    $this->initQueue();
    return $this->getResponse();
  }

  /**
   * Make cached request to Drupal.org API.
   *
   * @param string $url
   *   Drupal.org API URL.
   * @param string $type
   *   API Type.
   */
  public function makeRequest($url, $type = 'JSON') {
    $hash = hash('sha256', $url);
    $cid = 'drupal_marketplace_' . $hash;

    $cache = \Drupal::cache('drupal_marketplace')->get($cid);
    if ($cache) {
      $data = $cache->data;
    }
    else {
      $data = $this->makeRequestRaw($url);
      $tags = ['drupal_marketplace'];
      $expire = $this->config->get('expiration');
      \Drupal::cache('drupal_marketplace')->set($cid, $data, $expire, $tags);
    }
    if ($type === 'JSON') {
      $result = json_decode($data);
    }
    else {
      $result = new \SimpleXMLElement($data);
    }

    $this->setResponse($result);
  }

  /**
   * Helper to construct page URL.
   *
   * @param int $page
   *   Page number for API.
   *
   * @return string
   *   Specific Drupal.org API page.
   */
  public function constructPageUrl($page) {
    return str_replace('__PAGE__', $page, DRUPAL_MARKETPLACE_API_PAGE_URL);
  }

  /**
   * Helper to construct release URL.
   *
   * @param object $item
   *   Release JSON object.
   *
   * @return string
   *   Specific Drupal.org release API page.
   */
  public function constructModuleUrl($item) {
    return str_replace('__MODULE_MACHINE_NAME__', $item->field_project_machine_name, DRUPAL_MARKETPLACE_API_MODULE_URL);
  }

  /**
   * Getter for response var.
   *
   * @return mixed
   *   Response of Drupal.org API.
   */
  public function getResponse() {
    return $this->response;
  }

  /**
   * Setter for response var.
   *
   * @param mixed $response
   *   Response from Drupal.org API.
   */
  public function setResponse($response) {
    $this->response = $response;
  }

  /**
   * Queue initialisation.
   */
  private function initQueue() {
    $queue_factory = \Drupal::service('queue');
    $queue = $queue_factory->get('drupal-marketplace-page');

    for ($page = $this->getFirstPage(); $page <= $this->getLastPage(); $page++) {
      $queue->createItem($page);
    }
  }

  /**
   * Helper to parse first and last pages.
   */
  private function parsePages() {
    if (empty($response = $this->getResponse())) {
      return;
    }
    $last = $response->last;
    $first = $response->first;
    $pattern = '~\&page=(\d+)~';
    preg_match($pattern, $first, $matches);
    $this->setFirstPage((int) $matches[1]);
    preg_match($pattern, $last, $matches);
    $this->setLastPage((int) $matches[1]);
  }

  /**
   * Setter for query var.
   *
   * @param string $key
   *   Variable name.
   * @param string $value
   *   Variable value.
   *
   * @internal param array $query
   */
  public function setQuery($key, $value) {
    $this->query[$key] = $value;
  }

  /**
   * Getter for firstPage var.
   *
   * @return int
   *   First pages.
   */
  private function getFirstPage() {
    return $this->firstPage;
  }

  /**
   * Setter for firstPage var.
   */
  private function setFirstPage($firstPage) {
    $this->firstPage = $firstPage;
  }

  /**
   * Getter for lastPage var.
   *
   * @return int
   *   Last pages.
   */
  private function getLastPage() {
    return $this->lastPage;
  }

  /**
   * Setter for lastPage var.
   */
  private function setLastPage($lastPage) {
    $this->lastPage = $lastPage;
  }

  /**
   * Setter for headers var.
   *
   * @param string $key
   *   Variable name.
   * @param string $value
   *   Variable value.
   *
   * @internal param array $headers
   */
  private function setHeaders($key, $value) {
    $this->headers[$key] = $value;
  }

  /**
   * Make raw request to Drupal.org API.
   */
  private function makeRequestRaw($url) {
    $response = $this->client->request(
      'GET',
      $url,
      [
        'query' => $this->query,
        'headers' => $this->headers,
      ]
    );
    return $response->getBody()->getContents();
  }

}
