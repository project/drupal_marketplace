<?php

namespace Drupal\drupal_marketplace\Service;

use Drupal\Component\Datetime\Time;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\InfoParserInterface;
use Drupal\Core\ProxyClass\Extension\ModuleInstaller;
use Drupal\Core\FileTransfer\Local;
use Drupal\Core\Site\Settings;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Updater\Updater;
use Exception;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Class DrupalMarketPlaceInstaller.
 */
class DrupalMarketPlaceInstaller implements DrupalMarketPlaceInstallerInterface {

  use StringTranslationTrait;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;
  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;
  /**
   * Drupal\Core\File\FileSystemInterface definition.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;
  /**
   * Drupal\webprofiler\State\StateWrapper definition.
   *
   * @var \Drupal\webprofiler\State\StateWrapper
   */
  protected $state;
  /**
   * Drupal\Core\Extension\ModuleHandlerInterface definition.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  protected $time;

  protected $configFactory;

  protected $infoParser;

  protected $root;

  protected $sitePath;

  protected $moduleInstaller;

  /**
   * DrupalMarketPlaceInstaller constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   * @param \Drupal\Core\State\StateInterface $state
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   * @param \Drupal\Component\Datetime\Time $time
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   * @param \Drupal\Core\Extension\InfoParserInterface $infoParser
   * @param $root
   * @param $sitePath
   * @param \Drupal\Core\Extension\ModuleInstaller $moduleInstaller
   */
  public function __construct(
    RequestStack $request_stack,
    MessengerInterface $messenger,
    FileSystemInterface $file_system,
    StateInterface $state,
    ModuleHandlerInterface $module_handler,
    Time $time,
    ConfigFactoryInterface $configFactory,
    InfoParserInterface $infoParser,
    $root,
    $sitePath,
    ModuleInstaller $moduleInstaller
  ) {
    $this->requestStack = $request_stack;
    $this->messenger = $messenger;
    $this->fileSystem = $file_system;
    $this->state = $state;
    $this->moduleHandler = $module_handler;
    $this->time = $time;
    $this->configFactory = $configFactory;
    $this->infoParser = $infoParser;
    $this->root = $root;
    $this->sitePath = $sitePath;
    $this->moduleInstaller = $moduleInstaller;
  }

  public function installModule($link) {
    $local_cache = $this->getModuleFiles($link);
    if (!$local_cache) {
      $this->messenger->addError($this->t('Unable to retrieve Drupal project from %url.', ['%url' => $link]));
      return;
    }

    $directory = $this->extractDirectory();
    try {
      /** @var $archive */
      $archive = $this->extractArchive($local_cache, $directory);
    }
    catch (\Exception $e) {
      $this->messenger->addError($e->getMessage());
      return;
    }

    $files = $archive->listContents();
    if (!$files) {
      $this->messenger->addError($this->t('Provided archive contains no files.'));
      return;
    }

    // Unfortunately, we can only use the directory name to determine the
    // project name. Some archivers list the first file as the directory (i.e.,
    // MODULE/) and others list an actual file (i.e., MODULE/README.TXT).
    $project = strtok($files[0], '/\\');

    $archive_errors = $this->verifyUpdateArchive($project, $local_cache, $directory);
    if (!empty($archive_errors)) {
      $this->messenger->addError(array_shift($archive_errors));
      // @todo: Fix me in D8: We need a way to set multiple errors on the same
      // form element and have all of them appear!
      if (!empty($archive_errors)) {
        foreach ($archive_errors as $error) {
          $this->messenger->addError($error);
        }
      }
      return;
    }

    // Make sure the Updater registry is loaded.
    drupal_get_updaters();

    $project_location = $directory . '/' . $project;
    try {
      $updater = Updater::factory($project_location, $this->root);
    }
    catch (\Exception $e) {
      $this->messenger->addError($e->getMessage());
      return;
    }

    try {
      $project_title = Updater::getProjectTitle($project_location);
    }
    catch (\Exception $e) {
      $this->messenger->addError($e->getMessage());
      return;
    }

    if (!$project_title) {
      $this->messenger->addError($this->t('Unable to determine %project name.', ['%project' => $project]));
    }

    if ($updater->isInstalled()) {
      $this->messenger->addError($this->t('%project is already installed.', ['%project' => $project_title]));
      return;
    }

    $project_real_location = $this->fileSystem->realpath($project_location);

    // This process is inherently difficult to test therefore use a state flag.
    $test_authorize = FALSE;
    if (drupal_valid_test_ua()) {
      $test_authorize = $this->state->get('test_uploaders_via_prompt', FALSE);
    }
    // If the owner of the directory we extracted is the same as the owner of
    // our configuration directory (e.g. sites/default) where we're trying to
    // install the code, there's no need to prompt for FTP/SSH credentials.
    // Instead, we instantiate a Drupal\Core\FileTransfer\Local and invoke
    // update_authorize_run_install() directly.
    if (fileowner($project_real_location) == fileowner($this->sitePath) && !$test_authorize) {
      $filetransfer = new Local($this->root);
      $filetransfer->copyDirectory($project_real_location, $this->root . '/modules');
      $result = $this->moduleInstaller->install([$project]);
      return $result;
    }
  }

  /**
   * Copies a file from the specified URL to the temporary directory for updates.
   *
   * Returns the local path if the file has already been downloaded.
   *
   * @param $url
   *   The URL of the file on the server.
   *
   * @return string
   *   Path to local file.
   */
  function getModuleFiles($url) {
    $parsed_url = parse_url($url);
    $remote_schemes = ['http', 'https', 'ftp', 'ftps', 'smb', 'nfs'];
    if (!isset($parsed_url['scheme']) || !in_array($parsed_url['scheme'], $remote_schemes)) {
      // This is a local file, just return the path.
      return $this->fileSystem->realpath($url);
    }

    // Check the cache and download the file if needed.
    $cache_directory = $this->getTemporaryDirectory();
    $local = $cache_directory . '/' . $this->fileSystem->basename($parsed_url['path']);

    if (!file_exists($local) || $this->removeFilesIfStale($local)) {
      return system_retrieve_file($url, $local, FALSE, FILE_EXISTS_REPLACE);
    }
    else {
      return $local;
    }
  }

  /**
   * Returns the directory where update archive files should be cached.
   *
   * @param $create
   *   (optional) Whether to attempt to create the directory if it does not
   *   already exist. Defaults to TRUE.
   *
   * @return
   *   The full path to the temporary directory where update file archives should
   *   be cached.
   */
  function getTemporaryDirectory($create = TRUE) {
    $directory = &drupal_static(__FUNCTION__, '');
    if (empty($directory)) {
      $directory = 'temporary://drupal-marketplace-cache-' . $this->generateUniqueId();
      if ($create && !file_exists($directory)) {
        mkdir($directory);
      }
    }
    return $directory;
  }

  /**
   * Returns a short unique identifier for this Drupal installation.
   *
   * @return
   *   An eight character string uniquely identifying this Drupal installation.
   */
  function generateUniqueId() {
    $id = &drupal_static(__FUNCTION__, '');
    if (empty($id)) {
      $id = substr(hash('sha256', Settings::getHashSalt()), 0, 8);
    }
    return $id;
  }

  /**
   * Deletes stale files and directories from the update manager disk cache.
   *
   * Files and directories older than 6 hours and development snapshots older than
   * 5 minutes are considered stale. We only cache development snapshots for 5
   * minutes since otherwise updated snapshots might not be downloaded as
   * expected.
   *
   * When checking file ages, we need to use the ctime, not the mtime
   * (modification time) since many (all?) tar implementations go out of their way
   * to set the mtime on the files they create to the timestamps recorded in the
   * tarball. We want to see the last time the file was changed on disk, which is
   * left alone by tar and correctly set to the time the archive file was
   * unpacked.
   *
   * @param $path
   *   A string containing a file path or (streamwrapper) URI.
   */
  function removeFilesIfStale($path) {
    if (file_exists($path)) {
      $filectime = filectime($path);
      $max_age = $this->configFactory->get('system.file')->get('temporary_maximum_age');

      if ($this->time->getRequestTime() - $filectime > $max_age || (preg_match('/.*-dev\.(tar\.gz|zip)/i', $path) && $this->time->getRequestTime() - $filectime > 300)) {
        file_unmanaged_delete_recursive($path);
      }
    }
  }

  /**
   * Returns the directory where update archive files should be extracted.
   *
   * @param $create
   *   (optional) Whether to attempt to create the directory if it does not
   *   already exist. Defaults to TRUE.
   *
   * @return
   *   The full path to the temporary directory where update file archives should
   *   be extracted.
   */
  function extractDirectory($create = TRUE) {
    $directory = &drupal_static(__FUNCTION__, '');
    if (empty($directory)) {
      $directory = 'temporary://drupal-marketplace-extraction-' . $this->generateUniqueId();
      if ($create && !file_exists($directory)) {
        mkdir($directory);
      }
    }
    return $directory;
  }

  /**
   * Unpacks a downloaded archive file.
   *
   * @param string $file
   *   The filename of the archive you wish to extract.
   * @param string $directory
   *   The directory you wish to extract the archive into.
   *
   * @return Archiver
   *   The Archiver object used to extract the archive.
   *
   * @throws Exception
   */
  function extractArchive($file, $directory) {
    $archiver = archiver_get_archiver($file);
    if (!$archiver) {
      throw new Exception(t('Cannot extract %file, not a valid archive.', ['%file' => $file]));
    }

    // Remove the directory if it exists, otherwise it might contain a mixture of
    // old files mixed with the new files (e.g. in cases where files were removed
    // from a later release).
    $files = $archiver->listContents();

    // Unfortunately, we can only use the directory name to determine the project
    // name. Some archivers list the first file as the directory (i.e., MODULE/)
    // and others list an actual file (i.e., MODULE/README.TXT).
    $project = strtok($files[0], '/\\');

    $extract_location = $directory . '/' . $project;
    if (file_exists($extract_location)) {
      file_unmanaged_delete_recursive($extract_location);
    }

    $archiver->extract($directory);
    return $archiver;
  }

  /**
   * Implements hook_verify_update_archive().
   *
   * First, we ensure that the archive isn't a copy of Drupal core, which the
   * update manager does not yet support. See https://www.drupal.org/node/606592.
   *
   * Then, we make sure that at least one module included in the archive file has
   * an .info.yml file which claims that the code is compatible with the current
   * version of Drupal core.
   *
   * @see \Drupal\Core\Extension\ExtensionDiscovery
   */
  function verifyUpdateArchive($project, $archive_file, $directory) {
    $errors = [];

    // Parse all the .info.yml files and make sure at least one is compatible with
    // this version of Drupal core. If one is compatible, then the project as a
    // whole is considered compatible (since, for example, the project may ship
    // with some out-of-date modules that are not necessary for its overall
    // functionality).
    $compatible_project = FALSE;
    $incompatible = [];
    $files = file_scan_directory("$directory/$project", '/^' . DRUPAL_PHP_FUNCTION_PATTERN . '\.info.yml$/', ['key' => 'name', 'min_depth' => 0]);
    foreach ($files as $file) {
      // Get the .info.yml file for the module or theme this file belongs to.
      $info = $this->infoParser->parse($file->uri);

      // If the module or theme is incompatible with Drupal core, set an error.
      if (empty($info['core']) || $info['core'] != \Drupal::CORE_COMPATIBILITY) {
        $incompatible[] = !empty($info['name']) ? $info['name'] : t('Unknown');
      }
      else {
        $compatible_project = TRUE;
        break;
      }
    }

    if (empty($files)) {
      $errors[] = t('%archive_file does not contain any .info.yml files.', ['%archive_file' => $this->fileSystem->basename($archive_file)]);
    }
    elseif (!$compatible_project) {
      $errors[] = $this->formatPlural(
        count($incompatible),
        '%archive_file contains a version of %names that is not compatible with Drupal @version.',
        '%archive_file contains versions of modules or themes that are not compatible with Drupal @version: %names',
        ['@version' => \Drupal::CORE_COMPATIBILITY, '%archive_file' => $this->fileSystem->basename($archive_file), '%names' => implode(', ', $incompatible)]
      );
    }

    return $errors;
  }

}
