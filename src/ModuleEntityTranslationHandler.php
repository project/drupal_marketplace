<?php

namespace Drupal\drupal_marketplace;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for module_entity.
 */
class ModuleEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
