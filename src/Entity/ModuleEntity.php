<?php

namespace Drupal\drupal_marketplace\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Module entity entity.
 *
 * @ingroup drupal_marketplace
 *
 * @ContentEntityType(
 *   id = "module_entity",
 *   label = @Translation("Module entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\drupal_marketplace\ModuleEntityListBuilder",
 *     "views_data" = "Drupal\drupal_marketplace\Entity\ModuleEntityViewsData",
 *     "translation" = "Drupal\drupal_marketplace\ModuleEntityTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\drupal_marketplace\Form\ModuleEntityForm",
 *       "add" = "Drupal\drupal_marketplace\Form\ModuleEntityForm",
 *       "edit" = "Drupal\drupal_marketplace\Form\ModuleEntityForm",
 *       "delete" = "Drupal\drupal_marketplace\Form\ModuleEntityDeleteForm",
 *     },
 *     "access" = "Drupal\drupal_marketplace\ModuleEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\drupal_marketplace\ModuleEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "module_entity",
 *   data_table = "module_entity_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer module entity entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/module_entity/{module_entity}",
 *     "add-form" = "/admin/structure/module_entity/add",
 *     "edit-form" = "/admin/structure/module_entity/{module_entity}/edit",
 *     "delete-form" = "/admin/structure/module_entity/{module_entity}/delete",
 *     "collection" = "/admin/structure/module_entity",
 *   },
 *   field_ui_base_route = "module_entity.settings"
 * )
 */
class ModuleEntity extends ContentEntityBase implements ModuleEntityInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * Getter.
   */
  public function getSummary() {
    return $this->get('summary')->value;
  }

  /**
   * Setter.
   */
  public function setSummary($summary) {
    $this->set('summary', $summary);
  }

  /**
   * Getter.
   */
  public function getMachineName() {
    return $this->get('machine_name')->value;
  }

  /**
   * Setter.
   */
  public function setMachineName($machine_name) {
    $this->set('machine_name', $machine_name);
  }

  /**
   * Getter.
   */
  public function getComponents() {
    return $this->get('components')->value;
  }

  /**
   * Setter.
   */
  public function setComponents($components) {
    $this->set('components', $components);
  }

  /**
   * Getter.
   */
  public function getProjectType() {
    return $this->get('project_type')->value;
  }

  /**
   * Setter.
   */
  public function setProjectType($project_type) {
    $this->set('project_type', $project_type);
  }

  /**
   * Getter.
   */
  public function getCategories() {
    return $this->get('categories')->value;
  }

  /**
   * Setter.
   */
  public function setCategories($categories) {
    $this->set('categories', $categories);
  }

  /**
   * Getter.
   */
  public function getVersion() {
    return $this->get('version')->value;
  }

  /**
   * Setter.
   */
  public function setVersion($version) {
    $this->set('version', $version);
  }

  /**
   * Getter.
   */
  public function getDownloadLink() {
    return $this->get('download_link')->value;
  }

  /**
   * Setter.
   */
  public function setDownloadLink($download_link) {
    $this->set('download_link', $download_link);
  }

  /**
   * Getter.
   */
  public function getDocumentation() {
    return $this->get('documentation')->value;
  }

  /**
   * Setter.
   */
  public function setDocumentation($documentation) {
    $this->set('documentation', $documentation);
  }

  /**
   * Getter.
   */
  public function getImgs() {
    return $this->get('imgs')->value;
  }

  /**
   * Setter.
   */
  public function setImgs($imgs) {
    $this->set('imgs', $imgs);
  }

  /**
   * Getter.
   */
  public function getDownloadCount() {
    return $this->get('download_count')->value;
  }

  /**
   * Setter.
   */
  public function setDownloadCount($download_count) {
    $this->set('download_count', $download_count);
  }

  /**
   * Getter.
   */
  public function getSecurityAdvisoryCoverage() {
    return $this->get('security_advisory_coverage')->value;
  }

  /**
   * Setter.
   */
  public function setSecurityAdvisoryCoverage($security_advisory_coverage) {
    $this->set('security_advisory_coverage', $security_advisory_coverage);
  }

  /**
   * Getter.
   */
  public function getModuleCreated() {
    return $this->get('module_created')->value;
  }

  /**
   * Setter.
   */
  public function setModuleCreated($module_created) {
    $this->set('module_created', $module_created);
  }

  /**
   * Getter.
   */
  public function getModuleAuthor() {
    return $this->get('module_author')->value;
  }

  /**
   * Setter.
   */
  public function setModuleAuthor($module_author) {
    $this->set('module_author', $module_author);
  }

  /**
   * Getter.
   */
  public function getProjectUsage() {
    return $this->get('project_usage')->value;
  }

  /**
   * Setter.
   */
  public function setProjectUsage($project_usage) {
    $this->set('project_usage', $project_usage);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Module entity entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Module entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Module entity is published.'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['summary'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Summary'))
      ->setDescription(t('The Summary of the Module entity.'))
      ->setSettings([
        'max_length' => 500,
        'text_processing' => 0,
      ])
      ->setDefaultValue('');

    $fields['machine_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Machine name'))
      ->setDescription(t('The Machine name of the Module entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('');

    $fields['components'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Components'))
      ->setDescription(t('The Components of the Module entity.'))
      ->setSettings([
        'max_length' => 300,
        'text_processing' => 0,
      ])
      ->setDefaultValue('');

    $fields['project_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Project type'))
      ->setDescription(t('The Project type of the Module entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('');

    $fields['categories'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Module categories'))
      ->setDescription(t('Categories of the Module entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('');

    $fields['version'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Version'))
      ->setDescription(t('The Project type of the Module entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('');

    $fields['download_link'] = BaseFieldDefinition::create('uri')
      ->setLabel(t('Download link'))
      ->setDescription(t('The Download link of the Module entity.'));

    $fields['documentation'] = BaseFieldDefinition::create('uri')
      ->setLabel(t('Documentation link'))
      ->setDescription(t('The Documentation link of the Module entity.'));

    $fields['imgs'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Images'))
      ->setDescription(t('Images of the Module entity.'))
      ->setSettings([
        'max_length' => 200,
        'text_processing' => 0,
      ])
      ->setDefaultValue('');

    $fields['download_count'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Download count'))
      ->setDescription(t('Download count of the Module entity.'))
      ->setDefaultValue(0);

    $fields['security_advisory_coverage'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Security advisory coverage'))
      ->setDescription(t('The security advisory coverag of the Module entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('');

    $fields['module_created'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Module created timestamp'))
      ->setDescription(t('The module created timestamp coverag of the Module entity.'))
      ->setDefaultValue(0);

    $fields['module_author'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Module author advisory coverage'))
      ->setDescription(t('The module author of the Module entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('');

    $fields['project_usage'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Project usage'))
      ->setDescription(t('Project usage stat of the Module entity.'))
      ->setSettings([
        'max_length' => 100,
        'text_processing' => 0,
      ])
      ->setDefaultValue('');

    return $fields;
  }

}
