<?php

namespace Drupal\drupal_marketplace\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Module entity entities.
 */
class ModuleEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
