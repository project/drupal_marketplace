<?php

namespace Drupal\drupal_marketplace\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Module entity entities.
 *
 * @ingroup drupal_marketplace
 */
interface ModuleEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Module entity name.
   *
   * @return string
   *   Name of the Module entity.
   */
  public function getName();

  /**
   * Sets the Module entity name.
   *
   * @param string $name
   *   The Module entity name.
   *
   * @return \Drupal\drupal_marketplace\Entity\ModuleEntityInterface
   *   The called Module entity entity.
   */
  public function setName($name);

  /**
   * Gets the Module entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Module entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Module entity creation timestamp.
   *
   * @param int $timestamp
   *   The Module entity creation timestamp.
   *
   * @return \Drupal\drupal_marketplace\Entity\ModuleEntityInterface
   *   The called Module entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Module entity published status indicator.
   *
   * Unpublished Module entity are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Module entity is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Module entity.
   *
   * @param bool $published
   *   TRUE to set this Module entity to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\drupal_marketplace\Entity\ModuleEntityInterface
   *   The called Module entity entity.
   */
  public function setPublished($published);

  /**
   * Getter.
   */
  public function getSummary();

  /**
   * Setter.
   */
  public function setSummary($summary);

  /**
   * Getter.
   */
  public function getMachineName();

  /**
   * Setter.
   */
  public function setMachineName($machine_name);

  /**
   * Getter.
   */
  public function getComponents();

  /**
   * Setter.
   */
  public function setComponents($components);

  /**
   * Getter.
   */
  public function getProjectType();

  /**
   * Setter.
   */
  public function setProjectType($project_type);

  /**
   * Getter.
   */
  public function getCategories();

  /**
   * Setter.
   */
  public function setCategories($categories);

  /**
   * Getter.
   */
  public function getVersion();

  /**
   * Setter.
   */
  public function setVersion($version);

  /**
   * Getter.
   */
  public function getDownloadLink();

  /**
   * Setter.
   */
  public function setDownloadLink($download_link);

  /**
   * Getter.
   */
  public function getDocumentation();

  /**
   * Setter.
   */
  public function setDocumentation($documentation);

  /**
   * Getter.
   */
  public function getImgs();

  /**
   * Setter.
   */
  public function setImgs($imgs);

  /**
   * Getter.
   */
  public function getDownloadCount();

  /**
   * Setter.
   */
  public function setDownloadCount($download_count);

  /**
   * Getter.
   */
  public function getSecurityAdvisoryCoverage();

  /**
   * Setter.
   */
  public function setSecurityAdvisoryCoverage($security_advisory_coverage);

  /**
   * Getter.
   */
  public function getModuleCreated();

  /**
   * Setter.
   */
  public function setModuleCreated($module_created);

  /**
   * Getter.
   */
  public function getModuleAuthor();

  /**
   * Setter.
   */
  public function setModuleAuthor($module_author);

  /**
   * Getter.
   */
  public function getProjectUsage();

  /**
   * Setter.
   */
  public function setProjectUsage($project_usage);

}
