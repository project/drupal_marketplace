<?php

namespace Drupal\drupal_marketplace\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\drupal_marketplace\Entity\ModuleEntity;
use Drupal\drupal_marketplace\Service\DrupalMarketPlaceInstaller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class MarketPlaceController.
 */
class MarketplaceController extends ControllerBase {

  /**
   * The Market place insaller service.
   *
   * @var \Drupal\drupal_marketplace\Service\DrupalMarketPlaceInstaller
   */
  protected $marketPlaceInstaller;

  protected $entityTypeManager;

  /**
   * MarketplaceController constructor.
   *
   * @param \Drupal\drupal_marketplace\Service\DrupalMarketPlaceInstaller $marketPlaceInstaller
   */
  public function __construct(
    DrupalMarketPlaceInstaller $marketPlaceInstaller,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->marketPlaceInstaller = $marketPlaceInstaller;
    $this->entityTypeManager = $entityTypeManager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('drupal_marketplace.installer'),
      $container->get('entity_type.manager')
    );
  }
  /**
   * Marketplacepageaction.
   *
   * @return string
   *   Return Hello string.
   */
  public function marketplacePageAction() {
    return [
      '#type' => 'markup',
      '#attached' => [
        'library' => [
          'drupal_marketplace/drupal-marketplace-app',
        ],
      ],
      '#markup' => '<div id="app"></div>',
    ];
  }

  /**
   * Installs the module by provided name.
   *
   * @param $module_name
   *   A module name to install
   *
   * @return \Symfony\Component\HttpFoundation\Response
   */
  /**
   * @param $module_name
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function installModule($module_name) {
    $moduleEntityStorage = $this->entityTypeManager->getStorage('module_entity');
    $module = $moduleEntityStorage->loadByProperties(['machine_name' => $module_name]);
    $module = reset($module);
    if ($module instanceof ModuleEntity) {
      try {
        $link = $module->getDownloadLink();
        $response = $this->marketPlaceInstaller->installModule($link);
      }
      catch (\Exception $e) {
        $this->messenger->addError($e->getMessage());
      }
    }
    /** @var \Drupal\Core\Messenger\MessengerInterface $messenger */
    $messenger = \Drupal::service('messenger');
    if(empty($response)) {
      $response = $messenger->all();
    }
    return new JsonResponse($response);
  }

}
