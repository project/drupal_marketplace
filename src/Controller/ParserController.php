<?php

namespace Drupal\drupal_marketplace\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\drupal_marketplace\Service\DrupalMarketPlaceParserService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ParserController.
 *
 * @package Drupal\drupal_marketplace\Controller
 */
class ParserController extends ControllerBase {

  /**
   * @var \Drupal\drupal_marketplace\Service\DrupalMarketPlaceParserService
   */
  protected $parser;

  /**
   * MarketplaceController constructor.
   *
   * @param \Drupal\drupal_marketplace\Service\DrupalMarketPlaceInstaller $marketPlaceInstaller
   */
  public function __construct(DrupalMarketPlaceParserService $marketPlaceParserService) {
    $this->parser = $marketPlaceParserService;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('drupal_marketplace.parser')
    );
  }

  /**
   * Get modules from Drupal.org API.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Response from drupal.org API.
   */
  public function modules() {
    $this->parser->parseModules();
    $response = 'Queue initialised';
    return new Response($response);
  }

}
